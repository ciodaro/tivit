import unittest
from django.test import Client
from .models import FeiraLivre
import json

class SimpleTest(unittest.TestCase):

  def setUp(self):
    self.client = Client()
    

  def test_000_lista_de_feiras(self):
    print('Listar todas as Feiras...')
    response = self.client.get('/api/FeiraLivre/', content_type='application/json')
    self.assertEqual(response.status_code, 200)

  def test_001_busca_de_feiras_por_distrito(self):
    print('Buscar feiras do Distrito de Sapopemba')
    response = self.client.get('/api/FeiraLivre/?distrito=SAPOPEMBA', content_type='application/json')
    self.assertEqual(response.status_code, 200)

  def test_002_busca_de_feiras_por_regiao_5(self):
    print('Buscar feiras da Zona Leste')
    response = self.client.get('/api/FeiraLivre/?regiao_5=Leste', content_type='application/json')
    self.assertEqual(response.status_code, 200)

  def test_003_busca_de_feiras_por_nome_da_feira(self):
    print('Buscar feiras que tem jardim no nome')
    response = self.client.get('/api/FeiraLivre/?nome_feira=JARDIM', content_type='application/json')
    self.assertEqual(response.status_code, 200)

  def test_004_busca_de_feiras_por_bairro(self):
    print('Buscar feiras que tem vila no nome do bairro')
    response = self.client.get('/api/FeiraLivre/?bairro=vila', content_type='application/json')
    self.assertEqual(response.status_code, 200)


  def test_010_criar_uma_feira(self):
    print('Criar uma feira')
    data = {
      "are_ap": "3550308005209",
      "bairro": "JD SAPOPEMBA",
      "cod_dist": "78",
      "cod_subpref": "29",
      "distrito": "SAPOPEMBA",
      "lat": "-23613050",
      "lng": "-46498465",
      "logradouro": "RUA JOAO LOPES DE LIMA",
      "nome_feira": "FEIRA TESTE 888",
      "numero": "300.000000",
      "referencia": "TV AV SAPOPEMBA ALTURA 13000",
      "regiao_5": "Leste",
      "regiao_8": "Leste 1",
      "registro": "3112-7",
      "set_cens": "355030876000052",
      "subprefe": "VILA PRUDENTE"
    }

    response = self.client.post('/api/FeiraLivre/', json.dumps(data), content_type='application/json')
    self.assertEqual(response.status_code, 200)


  def test_020_erro_ao_criar_uma_feira_faltando_campos_obrigatorio(self):
    print('Não deixar criar uma feira por estar faltando dados obrigatórios')
    data = {
      "numero": "300.000000",
      "lng": "-46498465",
      "cod_dist": "78",
      "distrito": "SAPOPEMBA",
      "regiao_8": "Leste 1"
    }

    response = self.client.post('/api/FeiraLivre/', json.dumps(data), content_type='application/json')
    self.assertEqual(response.status_code, 400)
  

  def test_030_obter_uma_feira(self):
    print('Obter uma feira por ID')

    feira = FeiraLivre.objects.get(nome_feira='FEIRA TESTE 888')
    response = self.client.get('/api/FeiraLivre/%d' % feira.id, content_type='application/json')
    self.assertEqual(response.status_code, 200)

  def test_040_atualizar_uma_feira(self):
    print('Atualizar os dados de uma feira')

    data = {
      "bairro": "MOEMA"
    }

    feira = FeiraLivre.objects.get(nome_feira='FEIRA TESTE 888')

    response = self.client.put('/api/FeiraLivre/%d' % feira.id, json.dumps(data), content_type='application/json')
    self.assertEqual(response.status_code, 200)


  def test_100_excluir_uma_feira(self):
    print('Excluir uma feira')

    feira = FeiraLivre.objects.get(nome_feira='FEIRA TESTE 888')

    response = self.client.delete('/api/FeiraLivre/%d' % feira.id, content_type='application/json')
    self.assertEqual(response.status_code, 200)
    self.assertEqual(response.json()['success'], True)



    
