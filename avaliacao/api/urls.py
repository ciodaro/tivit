# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import FeiraLivreAPI

urlpatterns = [
  url(r'^FeiraLivre/?$', FeiraLivreAPI.as_view()),
  url(r'^FeiraLivre/(?P<id>[0-9A-Za-z_\-]+)/?$', FeiraLivreAPI.as_view()),
]